package com.davidups.beer.beer.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.davidups.beer.CoroutineTestRule
import com.davidups.beer.core.exception.Failure
import com.davidups.beer.core.extensions.empty
import com.davidups.beer.core.functional.Success
import com.davidups.beer.features.beer.data.models.view.BeerView
import com.davidups.beer.features.beer.domain.repository.BeerRepository
import com.davidups.beer.features.beer.domain.usescases.GetBeers
import com.davidups.beer.features.beer.views.viewmodel.BeerViewModel
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.test.KoinTest

class BeerViewModelTest : KoinTest {

    private lateinit var viewModel: BeerViewModel
    private lateinit var getBeers: GetBeers

    private val repository = mock<BeerRepository>()
    private val beersObserver = mock<Observer<MutableList<BeerView>>>()
    private val isErrorObserver = mock<Observer<Failure>>()
    private lateinit var expectedBeers: Success<MutableList<BeerView>>
    private lateinit var expectedError: Failure.Throwable

    @get:Rule
    var coroutinesRule = CoroutineTestRule()

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        getBeers = GetBeers(repository)
        viewModel = BeerViewModel(getBeers).apply {
            beers.observeForever(beersObserver)
            failure.observeForever(isErrorObserver)
        }

        expectedBeers = Success(mutableListOf())
        expectedError = Failure.Throwable(Throwable())
    }

    @Test
    fun `should emit beers on success and error`() =
        coroutinesRule.dispatcher.runBlockingTest {
            val channel = Channel<Success<MutableList<BeerView>>>()
            val flow = channel.consumeAsFlow()

            doReturn(flow)
                .whenever(repository)
                .getBeers()

            launch {
                channel.send(expectedBeers)
                channel.close(expectedError.throwable)
            }

            viewModel.beers()

            verify(beersObserver).onChanged(expectedBeers.data)
            verify(isErrorObserver).onChanged(expectedError)
        }

    @Test
    fun `should emit beers filter on success`() =
        coroutinesRule.dispatcher.runBlockingTest {

            viewModel.filterBeer(String.empty())

            verify(beersObserver).onChanged(expectedBeers.data)
        }

}
