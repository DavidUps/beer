package com.davidups.beer.beer

import com.davidups.beer.core.functional.Success
import com.davidups.beer.core.platform.NetworkHandler
import com.davidups.beer.features.beer.data.models.entity.BeerEntity
import com.davidups.beer.features.beer.data.models.view.BeerView
import com.davidups.beer.features.beer.data.services.BeerApi
import com.davidups.beer.features.beer.data.services.BeerService
import com.davidups.beer.features.beer.domain.repository.BeerRepository
import com.davidups.beer.features.beer.domain.repository.BeerRepositoryImp
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import org.amshove.kluent.`should be instance of`
import org.amshove.kluent.shouldBeEqualTo
import org.junit.Before
import org.junit.Test
import retrofit2.Response

class BeerRepositoryTest {

    private lateinit var networkHandler: NetworkHandler

    @Before
    fun setUp() {

        networkHandler = mock {
            onBlocking { isConnected } doReturn true
        }
    }

    @Test
    fun `should get beers on success`() = runBlocking {
        val beers = mutableListOf<BeerEntity>()

        val beerService = mock<BeerApi> {
            onBlocking { getBeer() } doReturn Response.success(beers)
        }

        beerService.getBeer().body() shouldBeEqualTo beers

        val service = mock<BeerService> {
            onBlocking { getBeer() } doReturn Response.success(beers)
        }

        val repository =
            BeerRepositoryImp(networkHandler, service)

        val flow = repository.getBeers()
        flow.collect {
            it.`should be instance of`<Success<MutableList<BeerView>>>()
            when (it) {
                is Success<MutableList<BeerView>> -> it.data shouldBeEqualTo beers.map {
                    it.toData().toView()
                }.toMutableList()
            }
        }
    }
}