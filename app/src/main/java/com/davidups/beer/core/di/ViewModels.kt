package com.davidups.beer.core.di

import com.davidups.beer.features.beer.views.viewmodel.BeerViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel { BeerViewModel(get()) }
}
