package com.davidups.beer.features.beer.data.services

import retrofit2.Retrofit

class BeerService(retrofit: Retrofit): BeerApi {

    private val beerApi by lazy {
        retrofit.create(BeerApi::class.java)
    }

    override suspend fun getBeer() = beerApi.getBeer()
}