package com.davidups.beer.features.beer.views.fragments

import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.GridLayoutManager
import com.davidups.beer.R
import com.davidups.beer.core.exception.Failure
import com.davidups.beer.core.extensions.empty
import com.davidups.beer.core.platform.BaseFragment
import com.davidups.beer.databinding.FragmentBeersBinding
import com.davidups.beer.features.beer.data.models.view.BeerView
import com.davidups.beer.features.beer.views.adapter.BeersAdapter
import com.davidups.beer.features.beer.views.viewmodel.BeerViewModel
import com.davidups.beer.core.extensions.observe
import com.davidups.beer.core.extensions.failure
import com.davidups.beer.core.extensions.hideKeyboard
import com.davidups.beer.core.platform.viewBinding.viewBinding
import org.koin.android.viewmodel.ext.android.viewModel

class BeersFragment : BaseFragment(R.layout.fragment_beers) {

    private val binding by viewBinding(FragmentBeersBinding::bind)
    private val beerViewModel by viewModel<BeerViewModel>()

    private val beersAdapter = BeersAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with(beerViewModel) {
            observe(beers, ::handleBeers)
            observe(showSpinner, ::handleShowSpinner)
            failure(failure, ::handleFailure)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
        initListeners()
    }

    private fun initView() {
        beerViewModel.beers()
        initSearchTil()
        binding.rvBeers.apply {
            layoutManager = GridLayoutManager(requireContext(), 2)
            adapter = beersAdapter
        }
    }

    private fun initSearchTil() {
        binding.tilSearch.clearFocus()
        binding.etSearch.clearFocus()
        requireActivity().window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    }

    private fun initListeners() {
        binding.etSearch.addTextChangedListener {
            beerViewModel.filterBeer(it.toString())
        }
        beersAdapter.beerListener = {
            navigate(BeersFragmentDirections.actionBeersFragmentToBeerFragment(it))
        }
        binding.tilSearch.setEndIconOnClickListener {
            binding.etSearch.clearFocus()
            binding.etSearch.setText(String.empty())
            binding.etSearch.hideKeyboard()
        }
    }


    private fun handleBeers(beers: MutableList<BeerView>?) {
        beers?.let {
            beersAdapter.collection = it.toList()
        }
    }

    private fun handleFailure(failure: Failure?) {
        when (failure) {
            is Failure.ServerError -> {
                Toast.makeText(requireContext(), "Error ${failure.errorCode}", Toast.LENGTH_SHORT)
                    .show()
            }
            is Failure.NetworkConnection -> {
                Toast.makeText(requireContext(), "No connection", Toast.LENGTH_SHORT).show()
            }
            else -> {
                Toast.makeText(requireContext(), "Error...", Toast.LENGTH_SHORT).show()
            }
        }
    }
}