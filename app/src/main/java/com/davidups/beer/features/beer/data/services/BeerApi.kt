package com.davidups.beer.features.beer.data.services

import com.davidups.beer.features.beer.data.models.entity.BeerEntity
import retrofit2.Response
import retrofit2.http.GET

internal interface BeerApi {

    companion object {
        private const val BEER = "beers"
    }

    @GET(BEER)
    suspend fun getBeer(): Response<MutableList<BeerEntity>>
}