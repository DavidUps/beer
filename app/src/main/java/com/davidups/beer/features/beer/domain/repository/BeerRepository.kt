package com.davidups.beer.features.beer.domain.repository

import com.davidups.beer.core.functional.State
import com.davidups.beer.features.beer.data.models.view.BeerView
import kotlinx.coroutines.flow.Flow

interface BeerRepository {

    fun getBeers(): Flow<State<MutableList<BeerView>>>

}