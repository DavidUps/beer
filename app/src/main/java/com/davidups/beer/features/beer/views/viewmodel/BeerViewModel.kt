package com.davidups.beer.features.beer.views.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.davidups.beer.core.exception.Failure
import com.davidups.beer.core.extensions.cancelIfActive
import com.davidups.beer.core.functional.Error
import com.davidups.beer.core.functional.Success
import com.davidups.beer.core.platform.BaseViewModel
import com.davidups.beer.features.beer.data.models.view.BeerView
import com.davidups.beer.features.beer.domain.usescases.GetBeers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch

class BeerViewModel(private val getBeers: GetBeers) : BaseViewModel() {

    private var beersJob: Job? = null

    private var beersList = mutableListOf<BeerView>()
    var beers = MutableLiveData<MutableList<BeerView>>()

    fun beers() {
        beersJob.cancelIfActive()
        beersJob = viewModelScope.launch {
            getBeers()
                .onStart { handleShowSpinner(true) }
                .onCompletion { handleShowSpinner(false) }
                .catch { failure -> handleFailure(Failure.Throwable(failure)) }
                .collect { result ->
                    when (result) {
                        is Success<MutableList<BeerView>> -> {
                            beersList = result.data
                            beers.value = result.data
                        }
                        is Error -> {
                            handleFailure(result.failure)
                        }
                    }
                }

        }
    }

    fun filterBeer(filter: String) {
        if (filter.isEmpty()) {
            beers.postValue(beersList)
        } else {
            val filterList = beersList
            val filter = filterList.filter {
                it.name.toUpperCase().contains(filter.toUpperCase())
            }.toMutableList()
            beers.postValue(filter)
        }

    }
}